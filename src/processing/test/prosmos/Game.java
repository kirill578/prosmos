package processing.test.prosmos;

import processing.core.PApplet;

public class Game {
	
	Matter[] cell;
	int cell_amount = 0;
	float ran_r, ran_x, ran_y;
	int mScreenWidth,mScreenHeight;
	boolean flag = true;
	Prosmos mApp;
	
	
	Game(Prosmos app,int srcWidth,int srcHeight,int type, int difficulty) {
		cell = new Matter[10000];
		mApp = app;
		mScreenHeight = srcHeight;
		mScreenWidth = srcWidth;

		cell[0] = new Matter(app.random(15, srcWidth - 15),app.random(15,srcHeight - 15), 15, 0, 0); // player
		cell_amount++;

		this.allocate_cell(app,difficulty * 10, 2, 30, 2);

	}

	public void allocate_cell(PApplet app,int num_cells, int min_r, int max_r,
			int max_speed) {
		for (int i = 1; i < num_cells; i++) {
			ran_r = app.random(min_r, max_r);
			do {
				flag = false;
				ran_x = app.random(max_r, mScreenWidth - max_r);
				ran_y = app.random(max_r, mScreenHeight - max_r);
				for (int j = 0; j < cell_amount; j++)
					if ((app.sqrt((ran_x - cell[j].x) * (ran_x - cell[j].x)
							+ (ran_y - cell[j].y) * (ran_y - cell[j].y))
							- ran_r - cell[j].radious) < 5)
						flag = true;
			} while (flag);
			cell[i] = new Matter(ran_x, ran_y, ran_r, app.random(-max_speed,
					max_speed) / (10 - num_cells*10), app.random(-max_speed, max_speed)
					/ (10 - num_cells*10));
			cell_amount++;
		}
	}

	public void begin() {
		mApp.background(220);

		this.calculate_collisions(mApp);

		// gravity
		if (mApp.GRAVITY != 0 && ( !mApp.MENU || !mApp.nowYouCanDrawMenu ) )
			for (int i = 0; i < cell_amount; i++)
				for (int j = i + 1; j < cell_amount; j++) {
					
					
					cell[j].push_to(
							cell[i].x,
							cell[i].y,
							mApp.gravity_force * (cell[i].radious * cell[i].radious)
									/ ((cell[i].distance(cell[j])) * (cell[i]
											.distance(cell[j]))));
					cell[i].push_to(
							cell[j].x,
							cell[j].y,
							mApp.gravity_force * (cell[j].radious * cell[j].radious)
									/ ((cell[i].distance(cell[j])) * (cell[i]
											.distance(cell[j]))));
					
					
					
				}

		for (int i = 0; i < cell_amount; i++) {
			if (cell[i].alive && ( !mApp.MENU || !mApp.nowYouCanDrawMenu ) ) {
				cell[i].wall(mScreenWidth,mScreenHeight);
				cell[i].move();

				// color
				float p = cell[i].radious / cell[0].radious;
				if (p > 1.5f)
					p = 1.5f;
				if (p < 0.5f)
					p = 0.5f;
				
				p -= 0.5f;
				
				int rr = 183; 
				int gr = 43;
				
				int rg = 100;
				int gg = 185;
				
				int r = (int) (( rr*p + rg*(1.0f - p) )/(p + (1.0f - p)));
				int g = (int) (( gr*p + gg*(1.0f - p) )/(p + (1.0f - p)));
				
				mApp.fill(r,g,55);
				
				
				//mApp.fill(81 * p * p, 255 - 94 * p * p, 55);
				
				if (i == 0)
					mApp.fill(60, 100, 250);

				cell[i].display(mApp,mApp.VECTOR);
			}
		}

	}

	public void calculate_collisions(Prosmos app) {
		for (int i = 0; i < cell_amount; i++)
			for (int j = i + 1; j < cell_amount; j++) {
				if ((cell[i].radious + cell[j].radious) > cell[i]
						.distance(cell[j]) && cell[i].alive && cell[j].alive) {
					float overlap;

					if (cell[i].radious > cell[j].radious) {
						overlap = (cell[i].radious + cell[j].radious - cell[i]
								.distance(cell[j])) / (2 * cell[j].radious);

						if (overlap > 1)
							overlap = 1;

						if (app.millis() - app.time > app.random(200, 450)) {
							app.time = app.millis();
							// Sound
							// if( cell[j].radious > 20 ) sfx3.trigger();
							// else if( cell[j].radious > 15 )
							// sfx2.trigger();
							// else if( cell[j].radious > 5 )
							// sfx1.trigger();
						}

						if (overlap == 1)
							cell[j].alive = false;

						cell[i].xspeed = (cell[i].xspeed
								* (cell[i].radious * cell[i].radious) + overlap
								* cell[j].xspeed
								* (cell[j].radious * cell[j].radious))
								/ ((cell[i].radious * cell[i].radious) + overlap
										* (cell[j].radious * cell[j].radious));
						cell[i].yspeed = (cell[i].yspeed * cell[i].radious
								* cell[i].radious + overlap
								* cell[j].yspeed * cell[j].radious
								* cell[j].radious)
								/ (cell[i].radious * cell[i].radious + overlap
										* cell[j].radious * cell[j].radious);

						cell[i].radious = (app.sqrt(cell[i].radious
								* cell[i].radious + overlap
								* cell[j].radious * cell[j].radious));
						cell[j].radious = (cell[j].radious * app.sqrt(1 - overlap));

					} else {

						overlap = (cell[j].radious + cell[i].radious - cell[j]
								.distance(cell[i])) / (2 * cell[i].radious);

						if (overlap > 1)
							overlap = 1;

						if (app.millis() - app.time > app.random(200, 450)) {
							// Sound
							app.time = app.millis();
							// if( cell[i].radious > 20 ) sfx3.trigger();
							// else if( cell[i].radious > 10 )
							// sfx2.trigger();
							// else if( cell[i].radious > 5 )
							// sfx1.trigger();
						}

						if (overlap == 1)
							cell[i].alive = false;

						cell[j].xspeed = (cell[j].xspeed * cell[j].radious
								* cell[j].radious + overlap
								* cell[i].xspeed * cell[i].radious
								* cell[i].radious)
								/ (cell[j].radious * cell[j].radious + overlap
										* cell[i].radious * cell[i].radious);
						cell[j].yspeed = (cell[j].yspeed * cell[j].radious
								* cell[j].radious + overlap
								* cell[i].yspeed * cell[i].radious
								* cell[i].radious)
								/ (cell[j].radious * cell[j].radious + overlap
										* cell[i].radious * cell[i].radious);

						cell[j].radious = (app.sqrt(cell[j].radious
								* cell[j].radious + overlap
								* cell[i].radious * cell[i].radious));
						cell[i].radious = (cell[i].radious * app.sqrt(1 - overlap));

					}

				}

			}

	}

}
