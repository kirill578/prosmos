package processing.test.prosmos;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import android.view.Display;
import android.view.KeyEvent;

public class Prosmos extends PApplet {

	
	boolean change_flag = false;

	int GRAVITY = 0;
	boolean VECTOR = false, MENU = true;
	int dif = 1;
	int scrHeight = 480;
	int scrWidth = 800;
	
	int gravity_force = 1;

	int time = millis();

	PFont font;

	Game thegame;

	boolean nowYouCanDrawMenu;
	

	public void setup() {
		smooth();

		Display display = getWindowManager().getDefaultDisplay();
		scrWidth = display.getWidth();
		scrHeight = display.getHeight();
		
		font = createFont("HelveticaLTStd-Blk.otf",48);
		textFont(font);

		thegame = new Game(this,scrWidth,scrHeight,0,dif);

		textAlign(CENTER);
		rectMode(CENTER);
	}

	public void draw() {

		if (MENU && nowYouCanDrawMenu) {
			show();
		} else {

			if (change_flag)
				thegame = new Game(this,scrWidth,scrHeight,0,dif);
			change_flag = false;
			thegame.begin();

			textSize(48);
			fill(0x1C, 0x1E, 0x2A);
			if (!thegame.cell[0].alive)
				text("Game Over", 150, scrHeight - 40);
			textSize(15);
			if (!thegame.cell[0].alive)
				text("Press anywhere to continue", 150, scrHeight - 20);
			
			if (MENU && !nowYouCanDrawMenu ) {
				nowYouCanDrawMenu = true;
			}
			
		}

		textSize(20);
		fill(0, 0, 0);
		rect(scrWidth - 30, scrHeight - 30, 30, 30);
		fill(255, 255, 255);
		text("M", scrWidth - 30, scrHeight - 22);

	}
	
	public void show() {
		MENU = true;
		pushMatrix();
		translate(scrWidth / 2, scrHeight / 2);
	
		PImage img = loadImage("logo.png");
		image(img,-img.width/2,-180);

		stroke(0,0);
		
		textSize(20);
		fill(0x1C, 0x1E, 0x2A);
		rect(0, -110, 280, 40);
		fill(255, 255, 255);
		text("Resume Game", 0, -110 + 7);

		fill(0x1C, 0x1E, 0x2A);
		rect(0, -65, 280, 40);
		fill(255, 255, 255);
		text("New Game", 0, -65 + 7);

		fill(0x1C, 0x1E, 0x2A);
		rect(0, -20, 280, 40);
		fill(255, 255, 255);
		text("Difficulty", -10, -20 + 7);
		text(dif, 50, -20 + 7);

		if (GRAVITY == 1 )
			fill(0x61, 0xB7, 0x30);
		else if ( GRAVITY == -1)
			fill(0x00, 0x00, 0x80);
		else
			fill(0xC0, 0x34, 0x33);
		
		rect(0, 25, 280, 40);
		fill(255, 255, 255);
		if (GRAVITY == 1 )
			text("Gravitation", 0, 25 + 7);
		else if ( GRAVITY == -1)
			text("Anti-Gravitation", 0, 25 + 7);
		else
			text("Gravitation", 0, 25 + 7);

		if (VECTOR)
			fill(0x61, 0xB7, 0x30);
		else
			fill(0xC0, 0x34, 0x33);
		rect(0, 70, 280, 40);
		fill(255, 255, 255);
		text("Momentum's Vector", 0, 70 + 7);

		
		if (GRAVITY != 0){
			fill(0x1C, 0x1E, 0x2A);
			rect(0, 115, 280, 40);
			fill(255, 255, 255);
			text("Gravity X" + gravity_force, 0, 115 + 7);
		}
		
		
		popMatrix();

		textSize(15);
		fill(255, 255, 255, 12);
		text("\u00a9 kirill Kulakov", 60, 25);

		textSize(15);
		fill(255, 255, 255, 12);
		text("Version 0.4", scrWidth - 50, 25);
	}

	public void mousePressed() {

		if (!MENU)
			thegame.cell[thegame.cell_amount++] = thegame.cell[0].accelerate(mouseX, mouseY);

		if (!thegame.cell[0].alive)
			thegame = new Game(this,scrWidth,scrHeight,0,dif);
		
		menucheck();
	}
	
	public void menucheck() {

		if (overRect(0, -110, 280, 40) && MENU)
			MENU = false;
		if (overRect(0, -65, 280, 40) && MENU) {
			MENU = false;
			thegame = new Game(this,scrWidth,scrHeight,0,dif);
		}
		if (overRect(0, -20, 280, 40) && MENU) {
			dif++;
			if (dif == 10)
				dif = 1;
			change_flag = true;
		}
		if (overRect(0, 25, 280, 40) && MENU){
			if(GRAVITY == 1){
				GRAVITY = -1;
				gravity_force *= -1;
			} else if(GRAVITY == -1) {
				GRAVITY = 0;
				gravity_force *= -1;
			} else
				GRAVITY = 1;
			
			
			
			nowYouCanDrawMenu = false;
		}
		if (overRect(0, 70, 280, 40) && MENU)
			VECTOR ^= true;
		
		if (GRAVITY != 0 ){
			if (overRect(0, 115, 280, 40) && MENU)
				gravity_force *= 2;
			
			if( gravity_force > 2*2*2*2*2*2*2*2 )
				gravity_force /= 2*2*2*2*2*2*2*2*2;
		}
		
		if (overRectN(scrWidth - 30, scrHeight - 30, 30, 30))
			MENU ^= true;

	}
	
	public boolean overRect(int x, int y, int width, int height) {
		if (mouseX >= x + scrWidth / 2 - width / 2
				&& mouseX <= x + scrWidth / 2 + width / 2
				&& mouseY >= y + scrHeight / 2 - height / 2
				&& mouseY <= y + scrHeight / 2 + height / 2) {
			return true;
		} else {
			return false;
		}
	}

	public boolean overRectN(int x, int y, int width, int height) {
		if (mouseX >= x - width / 2 && mouseX <= x + width / 2
				&& mouseY >= y - height / 2 && mouseY <= y + height / 2) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_MENU) {
	    	MENU ^= true;
            return true;
	    }
	    return super.onKeyUp(keyCode, event);
	}

}
