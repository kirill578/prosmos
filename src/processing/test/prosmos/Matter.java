package processing.test.prosmos;

import processing.core.PApplet;

public class Matter {

		float x, y, xspeed, yspeed, radious;
		int ONE_OUT_OFF = 50;
		boolean alive;

		Matter(float x_object, float y_object, float r_object, float r,
				float sx, float sy, float x_object_s, float y_object_s) {
			float m = sy / sx;
			float temp = (float) Math.sqrt(1 + m * m);

			if (sy > 0)
				y = y_object + ((r_object + r) * Math.abs(m)) / temp;
			else
				y = y_object - ((r_object + r) * Math.abs(m)) / temp;

			if (sx > 0)
				x = x_object + (r_object + r) / temp;
			else
				x = x_object - (r_object + r) / temp;

			radious = r;
			xspeed = (float) (sx * (Math.sqrt(ONE_OUT_OFF - 1)) + x_object_s);
			yspeed = (float) (sy * (Math.sqrt(ONE_OUT_OFF - 1)) + y_object_s);

			alive = true;
		}

		Matter(float x_object, float y_object, float r, float sx, float sy) {
			x = x_object;
			y = y_object;
			radious = r;
			xspeed = sx;
			yspeed = sy;

			alive = true;
		}

		public void display(PApplet app,boolean drawVector) {

			app.stroke(0,0);
			app.ellipse(x, y, radious * 2, radious * 2);
			if (drawVector) {
				app.fill(255, 50, 255);
				drawArrow(app,x, y, x + (xspeed * radious * radious) / 100, y
						+ (yspeed * radious * radious) / 100);
			}
		}
		
		public static void drawArrow(PApplet app,float x1, float y1, float x2, float y2) {
			app.stroke(0x1C, 0x1E, 0x2A);
			app.line(x1, y1, x2, y2);
			app.pushMatrix();
			app.translate(x2, y2);
			float a = (float) Math.atan2(x1 - x2, y2 - y1);
			app.rotate(a);
			app.line(0, 0, -2, -4);
			app.line(0, 0, 2, -4);
			app.popMatrix();
		}

		public void move() {
			x += xspeed / 10;
			y += yspeed / 10;
		}

		public void wall(int screenWidth,int screenHeight) {
			if (radious > x) {
				xspeed *= -1;
				x = radious;
			}
			if (x > screenWidth - radious) {
				xspeed *= -1;
				x = screenWidth - radious;
			}

			if (radious > y) {
				yspeed *= -1;
				y = radious;
			}
			if (y > screenHeight - radious) {
				yspeed *= -1;
				y = screenHeight - radious;
			}
		}

		public float distance(Matter a) {
			return (float) Math.sqrt((x - a.x) * (x - a.x) + (y - a.y) * (y - a.y));
		}

		public Matter accelerate(int xx, int yy) {
			float dx, dy, dt;

			dx = xx - x;
			dy = yy - y;

			dt = (float) Math.sqrt(dx * dx + dy * dy);

			xspeed -= dx / dt;
			yspeed -= dy / dt;

			float temp = radious;
			radious = (float) (radious * Math.sqrt(ONE_OUT_OFF - 1) / Math.sqrt(ONE_OUT_OFF));

			return new Matter(x, y, radious,
					(float) (temp * Math.sqrt(1)/ Math.sqrt(ONE_OUT_OFF)), dx / dt, dy / dt, xspeed, yspeed);
		}

		public void push_to(float xx, float yy, float s) {
			float dx, dy, dt;
			dx = xx - x;
			dy = yy - y;
			dt = (float) Math.sqrt(dx * dx + dy * dy);
			xspeed += ((dx / dt) * s) / 40;
			yspeed += ((dy / dt) * s) / 40;
		}


}
